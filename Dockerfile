FROM node:20.10.0-bullseye AS install

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm ci

FROM install AS build

COPY src ./src
COPY static ./static
COPY *.config.js .
COPY tsconfig.json .

RUN npm run build

FROM node:20.10.0-bullseye AS runtime

COPY package.json .
COPY --from=build /app/build ./build
COPY --from=build /app/node_modules ./node_modules

EXPOSE 3000

CMD ["node", "build/index.js"]

USER node